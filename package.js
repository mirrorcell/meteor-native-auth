Package.describe({
  name: 'mirrorcell:native-auth',
  version: '1.0.0',
  // Brief, one-line summary of the package.
  summary: 'Everything Needed to Run Google Plus and Facebook Oauth',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Cordova.depends({
  'cordova-plugin-facebook4': '1.4.0'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use(['ecmascript',
           'meteor-base',
           'accounts-base',
           'btafel:accounts-facebook-cordova',
           'hedcet:cordova-google-plus-native-sign-in@1.0.0',
           'check',
           "http",
           "peerlibrary:async"
  ]);

  api.imply(['check']);

  api.addFiles([
    'client/g_plus.js',
    'client/fb.js',
    'client/util.js',
    'client/shared.js',
    'client/styles.css'
  ], 'client');

  api.addFiles([
    'server/g_plus.js',
    'server/fb.js',
    'server/userPub.js',
    'server/config.accounts.js'
  ], 'server');

  api.export(["showFacebookSplash", "showGoogleSplash"]);

});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('native-auth');
  api.addFiles('native-auth-tests.js');
});
