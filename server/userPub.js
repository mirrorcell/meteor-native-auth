Meteor.publish('myUser', function() {
    if(this.userId) {
        return Meteor.users.find(this.userId);
    } else {
        return this.ready();
    }
});