if(!(Meteor.settings.google.clientId && Meteor.settings.google.clientSecret )) {
    throw new Meteor.Error("Required-Values", "Google Client ID or Client Secret Were not Found");
}

var google = {
    clientId: Meteor.settings.google.clientId,
    clientSecret: Meteor.settings.google.clientSecret
};


var Future = Npm.require('fibers/future');

Meteor.methods({
    linkGPlus: function(req) {
        console.log("LINKING", req, google);
        var future = new Future();

        var user = null;

        if (!req.cordova_g_plus)
            return undefined;

        check(req, {
            cordova_g_plus: Boolean,
            email: String,
            oAuthToken: String,
            profile: Match.Any,
            sub: String
        });

        if(!this.userId) {
            return
        } else {
            user = Meteor.users.findOne(this.userId);
        }

        if (user) {
            var res = Meteor.http.get("https://www.googleapis.com/oauth2/v3/userinfo", {
                headers: {
                    "User-Agent": "Meteor/1.0"
                },

                params: {
                    access_token: req.oAuthToken
                }
            });

            if (res.error) throw res.error;
            else {
                if ( /* req.email == res.data.email && */ req.sub == res.data.sub) {
                    var googleResponse = _.pick(res.data, "email", "email_verified", "family_name", "gender", "given_name", "locale", "name", "picture", "profile", "sub");

                    console.log("GOOGLE REQUEST", req);
                    console.log("GOOGLE RESPONSE", res);

                    googleResponse["accessToken"] = req.oAuthToken;
                    googleResponse["id"] = req.sub;

                    if (typeof(googleResponse["email"]) == "undefined") {
                        googleResponse["email"] = req.email;
                    }

                    var updateObject = {
                        services: {
                            google: googleResponse
                        }
                    };

                    Meteor.users.update(this.userId, {
                        $set : {
                            'services.google' : updateObject.services.google
                        }
                    }, function(err, docs) {
                        console.log("ERROR", err, "DOCS", docs);
                        future.return(err, docs);
                    })

                } else throw new Meteor.Error(422, "AccessToken MISMATCH  package");
            }
        } else {
            future.return("ERROR");
        }

        return future.wait();
    },
    getGPlusRefreshToken : function() {
        var user = Meteor.users.findOne(this.userId);
        if(!user) {
            throw new Meteor.Error("not-found", "COuld not find user");
        }

        console.log("SEnding", user.services.google.accessToken, google.clientId, google.clientSecret);

        var res = Meteor.http.get("https://accounts.google.com/o/oauth2/token", {
            headers: {
                "User-Agent": "Meteor/1.0"
            },

            params: {
                code: user.services.google.accessToken,
                client_id : google.clientId,
                client_secret : google.clientSecret,
                redirect_url: 'localhost:3000/oauth2/gplus/' + user._id + '/',
                grant_type: "authorization_code"
            }
        });

        if(res) {
            console.log("RESPONSE", res);
        } else {
            console.log("SHIT");
        }
    }
});