if(!(Meteor.settings.google.clientId && Meteor.settings.google.clientSecret )) {
    throw new Meteor.Error("Required-Values", "Google Client ID or Client Secret Were not Found");
}

var google = {
    clientId: Meteor.settings.google.clientId,
    clientSecret: Meteor.settings.google.clientSecret
};

Meteor.startup(function() {

    Accounts.loginServiceConfiguration.remove({
        service: "google"
    });

    Accounts.loginServiceConfiguration.insert({
        service: "google",
        clientId: google.clientId,
        secret: google.clientSecret
    });

});

//Accounts.config({
//    // forbidClientAccountCreation: true,
//    loginExpirationInDays: 0,
//    // restrictCreationByEmailDomain: function(emailId) {
//    //     var domainAllowed = ["hedcet.com"];
//    //     var domain = emailId.slice(emailId.lastIndexOf("@") + 1);
//    //     return _.contains(domainAllowed, domain);
//    // },
//    // sendVerificationEmail: true
//});
//
//Accounts.onCreateUser(function(opts, user) {
//    var res = Meteor.http.get("https://www.googleapis.com/oauth2/v3/userinfo", {
//        headers: {
//            "User-Agent": "Meteor/1.0"
//        },
//
//        params: {
//            access_token: user.services.google.accessToken
//        }
//    });
//
//    if (res.error)
//        throw res.error;
//
//    user.profile = _.pick(res.data, "email", "email_verified", "gender", "locale", "name", "picture", "sub");
//
//    return user;
//});