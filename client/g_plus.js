GPlusOauth = {
    plugin : null,
    getPlugin : function() {
        if(window && window.plugins && window.plugins.googleplus && _.isObject(window.plugins.googleplus)) {
            plugin = window.plugins.googleplus;
        }
    }
};

Meteor.linkGoogle = function(request, callback) {
    if(window && window.plugins && window.plugins.googleplus) {

        if(Meteor.user() && Meteor.user().services && Meteor.user().services.google) {
            return;
        }

        var opt = { offline : true };

        if(request.scopes) {
            opt.scopes = request.scopes
        }

        window.plugins.googleplus.login(opt, function(response) {

                request.scopes && delete request.scopes;

                console.log("RESPONSE", response);

                request.email = response.email;
                request.oAuthToken = response.oauthToken;
                request.sub = response.userId;

                Meteor.call('linkGPlus', request, callback);
            },
            function(error) {
                if (callback && (typeof callback == "function")) callback(error);
                else alert(error);
            }
        );
    }
};

Meteor.loginWithGoogle = function(request, callback) {
    /*
     * @function cordova_g_plus
     * @summary Function to call native google plus login only available in cordova apps
     * @memberof Meteor
     * @param {Object} request an object with google plus login details
     * @param {Boolean} request.cordova_g_plus `request.cordova_g_plus` expected to be true to start native google plus login
     * @param {Array} request.profile Is an array of profile properties required, eg. `["email", "email_verified", "gender"]`
     * @param {Function} callback `callback` function can have one argument `error` which will be containing the details of error if any
     */

    var opt = { offline : true };

    if(request.scopes) {
        opt.scopes = request.scopes
    }

    window.plugins.googleplus.login(opt, function(response) {
            request.scopes && delete request.scopes;

            console.log("RESPONSE", response);

            request.email = response.email;
            request.oAuthToken = response.oauthToken;
            request.sub = response.userId;

            Accounts.callLoginMethod({ // call cordova_g_plus SignIn handler @ server
                methodArguments: [request],
                userCallback: callback
            });
        },
        function(error) {
            if (callback && (typeof callback == "function")) callback(error);
            else alert(error);
        }
    );
};


Meteor.loginWithGoogleSplash = function(options, callback) {
    console.log("LoginWithGoogleSPlash: LOgging in with", options);
    showGoogleSplash();

    var opt = _.clone(options);
    opt.cordova_g_plus = true;

    Meteor.loginWithGoogle(opt, function(error) {
        hideGoogleSplash();
        callback(error);
    });
};

var showGoogleSplash = function() {
    var elemDiv = document.createElement('div');
    var gIconDiv = document.createElement('h1');

    gIconDiv.innerText = "Google";
    gIconDiv.style.cssText= "position: absolute; top:50%; left: 50%; color:white; margin-right: -50%; transform: translate(-50%, -50%);";

    elemDiv.style.cssText = 'position:absolute;width:100%;height:0%;z-index:100;background:#dd4b39;bottom:0; left:0';
    elemDiv.className = "extend-top";
    elemDiv.appendChild(gIconDiv);

    document.body.appendChild(elemDiv);
};

var hideGoogleSplash = function() {
    var el = $('.extend-top');
    el.removeClass('extend-top');
    el.addClass('shrink-bottom');
};


Meteor.startup(function() {
    GPlusOauth.getPlugin();
});