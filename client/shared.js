var origLogout = Meteor.logout;

Meteor.logout = function(myCallback) {
        var user = Meteor.user(),
            tasks = [],
            self = this,
            callback = myCallback || function() {};

        console.log("Started logout");

        if(!user) return origLogout(callback);
        if(!Meteor.isCordova) return origLogout(callback);

        var services = user.services;

        if(!services) return origLogout(callback);

        console.log("Checking Cordova Meteor Login Methods for User", Meteor.user());

        if(GPlusOauth.plugin && services.google) {
            console.log("We have g plus login, lets logout");

            tasks.push(function(cb) {
                GPlusOauth.logout(cb);
            });
        }

        if(facebookConnectPlugin && services.facebook) {
            console.log("We have a facebook login, lets logout");

            tasks.push(function(cb) {
                facebookConnectPlugin.logout(cb, cb);
            });
        }

        if(tasks.length > 0) {
            console.log("Time to async");
            async.parallel(tasks, function (err, res) {
                if(err) {
                    //TODO : Figure out what to do for this
                    alert(err);
                }
                console.log("Finished logging out", err, res);
                return origLogout(callback);
            })
        } else {
            console.log("No async");
            return origLogout(callback);
        }
};